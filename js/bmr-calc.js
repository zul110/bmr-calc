const bmrHeightInput = $('#bmr-calc-height-input');
const bmrWeightInput = $('#bmr-calc-weight-input');
const bmrAgeInput = $('#bmr-calc-age-input');
const bmrCaloriesInput = $('#bmr-calc-calories-input');

const bmrGenderSelector = $('#bmr-calc-gender-selector');
const bmrActivitiesSelector = $('#bmr-calc-activities-selector');

const bmrResultCaloriesBurnt = $('#bmr-calc-calories-burnt');

const bmrErrorsList = $('#bmr-calc-errors-list');
const bmrHeightError = $('#bmr-calc-height-error');
const bmrWeightError = $('#bmr-calc-weight-error');
const bmrAgeError = $('#bmr-calc-age-error');
const bmrGenderError = $('#bmr-calc-gender-error');
const bmrActivitiesError = $('#bmr-calc-activities-error');

const bmrSubmitButton = $('#btn_Submit_BmrCalc_Right');
const bmrResultButton = $('#bmr-calc-result-return-button');

const bmrErrorTexts = {
    height:   'Please enter your height (between 90cm and 245cm)',
    weight:   'Please enter your weight (between 25kg and 275kg)',
    age:      'Please enter your age (between 18 and 99)',
    gender:   'Please enter your gender',
    activity: 'Please enter your activity level',
};

const bmrErrorText = 'الرجاء تعبئة الاجابة';

bmrSubmitButton.click(() => {
    const height = bmrHeightInput.val();
    const weight = bmrWeightInput.val();
    const age = bmrAgeInput.val();
    const calories = bmrCaloriesInput.val();

    const gender = bmrGenderSelector.val();
    const activities = bmrActivitiesSelector.val();

    const errors = [];
    bmrHeightError.html('');
    bmrWeightError.html('');
    bmrAgeError.html('');
    bmrGenderError.html('');
    bmrActivitiesError.html('');

    if(!height || height == 0 || (height < 90 || height > 245)) {
        errors.push(bmrErrorTexts.height);
        bmrHeightError.html(bmrErrorText);
    }

    if(!weight || weight == 0 || (weight < 25 || weight > 275)) {
        errors.push(bmrErrorTexts.weight);
        bmrWeightError.html(bmrErrorText);
    }

    if(!age || age == 0 || (age < 18 || age > 99)) {
        errors.push(bmrErrorTexts.age);
        bmrAgeError.html(bmrErrorText);
    }

    if(!gender || gender == 0) {
        errors.push(bmrErrorTexts.gender);
        bmrGenderError.html(bmrErrorText);
    }

    if(!activities || activities == -1) {
        errors.push(bmrErrorTexts.activity);
        bmrActivitiesError.html(bmrErrorText);
    }

    const bmr = calcBmr(height, weight, gender, age);

    const caloriesBurnt = calcBmrCaloriesBurnt(bmr, activities, calories);

    bmrResultCaloriesBurnt.html(caloriesBurnt + ' سعرة حرارية');

    if ((!height || !weight || !age || !gender || !activities) || (height == 0 || weight == 0 || age == 0 || gender == 0 || activities == -1)) {
        bmrHideResults();
    } else {
        bmrShowResults();
    }
});

const bmrShowErrors = errors => {
    bmrHideErrors();

    errors.forEach(error => {
        bmrErrorsList.append(`<li class='alert-danger error-item'>${error}</li>`);
    });
};

const bmrHideErrors = () => bmrErrorsList.empty();

const calcBmr = (height, weight, gender, age) => {
    const s = gender === 'm' ? 5 : -161;
    return (10 * weight) + (6.25 * height) - (5 * age) + s;
};

const calcBmrCaloriesBurnt = (bmr, level, calories = 0) => {
    const levels = [
        1,
        1.2,
        1.375,
        1.55,
        1.725
    ];

    const caloriesPerDay = (levels[+level] || levels[0]) * bmr;

    return caloriesPerDay - calories;
}

bmrResultButton.click(() => {
    bmrHideResults();
});

const bmrHideResults = () => {
    $('.bmr-calc-result').css('display', 'none');
    $('.calcWzDescCnts').css('display', 'block');
};

const bmrShowResults = () => {
    $('.bmr-calc-result').css('display', 'flex');
    $('.calcWzDescCnts').css('display', 'none');
};